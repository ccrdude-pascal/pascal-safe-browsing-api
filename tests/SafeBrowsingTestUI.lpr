{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Project file for test project with user interface)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-15  pk  ---  Created test project.
// *****************************************************************************
   )
}

program SafeBrowsingTestUI;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

uses
   {$IFDEF UNIX}
   cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
   athreads,
   {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   SysUtils,
   Forms,
   SafeBrowsingTest.UI.FormMain;

{$R *.res}

   function TestVendorName: string;
   begin
      Result := 'Safer-Networking Ltd';
   end;

   function TestAppName: string;
   begin
      Result := 'Safe Browsing Test';
   end;

begin
   {$IFDEF DEBUG}
   ForceDirectories(ExtractFilePath(ParamStr(0)) + 'debug');
   {$if declared(UseHeapTrace)}
   if UseHeaptrace then begin
      GlobalSkipIfNoLeaks := True; // supported as of debugger version 3.1.1
      SetHeapTraceOutput(IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)) + 'debug') + 'heap.log');
   end;
   {$ifend}
   {$ENDIF DEBUG}
   OnGetVendorName := TestVendorName;
   OnGetApplicationName := TestAppName;
   RequireDerivedFormResource := True;
  Application.Title:='Goole Safe Browsing V4 Test User Interface';
  Application.Scaled:=True;
   Application.Initialize;
   Application.CreateForm(TFormSafeBrowsingLookupTest, FormSafeBrowsingLookupTest);
   Application.Run;
end.
