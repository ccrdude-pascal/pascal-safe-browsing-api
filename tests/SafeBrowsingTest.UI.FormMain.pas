{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Visual form to test Google Safe Browsing API code.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Added storage for API Key to have it outside of repository.
// 2023-03-16  pk  ---  Added ThreatLists.
// 2023-03-15  pk  ---  Created simple form.
// *****************************************************************************
   )
}

unit SafeBrowsingTest.UI.FormMain;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ComCtrls, ExtCtrls,
   IniFiles,
   API.Google.SafeBrowsing.V4,
   API.Google.SafeBrowsing.V4.Types,
   API.Google.SafeBrowsing.V4.ThreatLists,
   API.Google.SafeBrowsing.V4.ThreatMatches,
   {$IFDEF FPC}
   Layers.JSON.FreePascal,
   {$ENDIF FPC}
   Layers.Transport.Synapse,
   Layers.JSON.LeonKon,
   Layers.Transport.Indy10,
   {$IFNDEF FPC}
   Layers.Transport.ICS,
   {$ENDIF FPC}
   {$IFDEF DEBUG}
   Layers.Transport.Debugger.UI,
   Layers.Transport.Debugger.List,
   {$ENDIF DEBUG}
   OVM.ListView;

type

   { TSafeBrowsingConfigIniFile }

   TSafeBrowsingConfigIniFile = class(TMemIniFile)
   public
      constructor Create;
   public
      class function GetAPIKey: string;
      class procedure SetAPIKey(AKey: string);
   end;

   { TFormSafeBrowsingLookupTest }

   TFormSafeBrowsingLookupTest = class(TForm)
      bnThreatMatchesFind: TButton;
      bnThreatListsList: TButton;
      groupJSON: TGroupBox;
      groupTransport: TGroupBox;
      lvThreatMatchesFind: TListView;
      lvThreatListsList: TListView;
      memoThreatMatchesFindURLs: TMemo;
      pcMethods: TPageControl;
      rbLayeredTransportIndy10: TRadioButton;
      rbLayeredJSONLeonKon: TRadioButton;
      rbLayeredJSONFreePascal: TRadioButton;
      rbLayeredTransportICS: TRadioButton;
      rbLayeredTransportSynapse: TRadioButton;
      tabSettings: TTabSheet;
      tabThreatListsList: TTabSheet;
      tabThreatMatchesFind: TTabSheet;
      procedure bnThreatListsListClick({%H-}Sender: TObject);
      procedure bnThreatMatchesFindClick({%H-}Sender: TObject);
      procedure FormChangeBounds(Sender: TObject);
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormResize({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
   private
      FProxyHost: string;
      FProxyPort: integer;
      FSafeBrowsingAPIKey: string;
      procedure SelectLayers;
   public

   end;

var
   FormSafeBrowsingLookupTest: TFormSafeBrowsingLookupTest;

implementation

{$R *.lfm}

{ TSafeBrowsingConfigIniFile }

constructor TSafeBrowsingConfigIniFile.Create;
begin
   inherited Create(GetAppConfigDir(False) + 'safebrowsing.ini');
end;

class function TSafeBrowsingConfigIniFile.GetAPIKey: string;
var
   i: TSafeBrowsingConfigIniFile;
begin
   i := TSafeBrowsingConfigIniFile.Create;
   try
      Result := i.ReadString('SafeBrowsing', 'APIKey', '');
   finally
      i.Free;
   end;
end;

class procedure TSafeBrowsingConfigIniFile.SetAPIKey(AKey: string);
var
   i: TSafeBrowsingConfigIniFile;
begin
   i := TSafeBrowsingConfigIniFile.Create;
   try
      i.WriteString('SafeBrowsing', 'APIKey', AKey);
      i.UpdateFile;
   finally
      i.Free;
   end;
end;

{ TFormSafeBrowsingLookupTest }

procedure TFormSafeBrowsingLookupTest.bnThreatMatchesFindClick(Sender: TObject);
var
   api: TSafeBrowsingV4API;
   tm: TThreatMatches;
   m: TThreatMatch;
   li: TListItem;
begin
   SelectLayers;
   api := TSafeBrowsingV4API.Create;
   try
      api.APIKey := Self.FSafeBrowsingAPIKey;
      api.ProxyHost := Self.FProxyHost;
      api.ProxyPort := Self.FProxyPort;
      tm := TThreatMatches.Create;
      try
         api.ThreatMatchesFindURLs(memoThreatMatchesFindURLs.Lines.ToStringArray, tm, [sbttMalware, sbttSocialEngineering, sbttUnwantedSoftware, sbttPotentiallyHarmfulApplication]);
         lvThreatMatchesFind.Items.BeginUpdate;
         try
            lvThreatMatchesFind.Items.Clear;
            for m in tm do begin
               if lvThreatMatchesFind.Items.Count = 0 then begin
                  lvThreatMatchesFind.UpdateColumnsForClass(m);
               end;
               li := lvThreatMatchesFind.Items.Add;
               lvThreatMatchesFind.UpdateListItemForObject(li, m);
            end;
         finally
            lvThreatMatchesFind.Items.EndUpdate;
         end;
      finally
         tm.Free;
      end;
   finally
      api.Free;
   end;
end;

procedure TFormSafeBrowsingLookupTest.FormChangeBounds(Sender: TObject);
begin
   FormResize(Sender);
end;

procedure TFormSafeBrowsingLookupTest.bnThreatListsListClick(Sender: TObject);
var
   api: TSafeBrowsingV4API;
   tl: TThreatLists;
   l: TThreatList;
   li: TListItem;
begin
   SelectLayers;
   api := TSafeBrowsingV4API.Create;
   try
      api.APIKey := Self.FSafeBrowsingAPIKey;
      api.ProxyHost := Self.FProxyHost;
      api.ProxyPort := Self.FProxyPort;
      tl := TThreatLists.Create;
      try
         api.ThreatListsList(tl);
         lvThreatListsList.Items.BeginUpdate;
         try
            lvThreatListsList.Items.Clear;
            for l in tl do begin
               if lvThreatListsList.Items.Count = 0 then begin
                  lvThreatListsList.UpdateColumnsForClass(l);
               end;
               li := lvThreatListsList.Items.Add;
               lvThreatListsList.UpdateListItemForObject(li, l);
            end;
         finally
            lvThreatListsList.Items.EndUpdate;
         end;
      finally
         tl.Free;
      end;
   finally
      api.Free;
   end;
end;

procedure TFormSafeBrowsingLookupTest.FormCreate(Sender: TObject);
begin
   Self.FSafeBrowsingAPIKey := TSafeBrowsingConfigIniFile.GetAPIKey;
   Self.FProxyHost := '127.0.0.1';
   Self.FProxyPort := 8888;
   Self.FProxyHost := '';
   Self.FProxyPort := 0;
end;

procedure TFormSafeBrowsingLookupTest.FormResize(Sender: TObject);
begin
   {$IFDEF DEBUG}
   TFormTransportDebugger.Instance.Show;
   TFormTransportDebugger.Instance.Left := Self.Left + Self.Width;
   {$ENDIF DEBUG}
end;

procedure TFormSafeBrowsingLookupTest.FormShow(Sender: TObject);
begin
   {$IFDEF FPC}
   rbLayeredJSONFreePascal.Checked := true;
   {$ENDIF FPC}
   FormResize(Self);
   SelectLayers;
   if Length(Self.FSafeBrowsingAPIKey) = 0 then begin
      if InputQuery('Safe Browsing API Key', 'Safe Browsing requires an API Key you get from Google.'#13#10'Check README.md for details on optaining one.', Self.FSafeBrowsingAPIKey) then begin
         TSafeBrowsingConfigIniFile.SetAPIKey(Self.FSafeBrowsingAPIKey);
      end else begin
         Close;
      end;
   end;
end;

procedure TFormSafeBrowsingLookupTest.SelectLayers;
begin
   if rbLayeredJSONLeonKon.Checked then begin
      LayeredPickJSONLeonKon;
   end;
   {$IFDEF FPC}
   if rbLayeredJSONFreePascal.Checked then begin
      LayeredPickJSONFreePascal;
   end;
   {$ELSE FPC}
   rbLayeredJSONFreePascal.Enabled := false;
   {$ENDIF FPC}
   if rbLayeredTransportSynapse.Checked then begin
      LayeredPickTransportSynapse;
   end;
   if rbLayeredTransportIndy10.Checked then begin
      LayeredPickTransportIndy10;
   end;
   {$IFDEF FPC}
   rbLayeredTransportICS.Enabled := false;
   {$ELSE FPC}
   if rbLayeredTransportICS.Checked then begin
      LayeredPickTransportICS;
   end;
   {$ENDIF FPC}
end;

end.
