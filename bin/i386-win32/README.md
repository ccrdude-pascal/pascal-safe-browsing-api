# Instructions

You need to place the following OpenSSL 3 binaries into this folder:

* `legacy.dll`
* `libcrypto-3.dll`
* `libssl-3.dll`

You can get precompiled binaries [e.g. here](http://wiki.overbyte.eu/wiki/index.php/ICS_Download#Download_OpenSSL_Binaries_.28required_for_SSL-enabled_components.29).