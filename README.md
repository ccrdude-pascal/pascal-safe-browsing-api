# Lazarus API for Google Safe Browsing V4

## Description

This repository includes the FreePascal code required to access to Google Safe Browsing API, version 4.

## Features

* Implements the *Lookup API (v4)*

## Requirements

* You need to place OpenSSL files into the binary folder (see README.md there), either
  * OpenSSL 3 libraries if you want use Synpase, or
  * OpenSSL 1 libraries if you want to use Indy 10.
* You need to get a Google API key.

## Obtaining a Google API Key

You need to create a project in Google Cloud, create an API key for this project, and enable the Safe Browsing API to be able to use the key for Safe Browsing.

* [Create a Google account](https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com%2FManageAccount%3Fnc%3D1&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp) if you do not have one.
* Open [Credentials](https://console.cloud.google.com/apis/credentials) page.
* Create a project.
* Click +, then pick API key.
* Copy key, close window.
* Switch to [API List](https://console.cloud.google.com/apis/dashboard)
* Click + to add.
* Search for Safe Browsing API
* Click Activate on product details page.
