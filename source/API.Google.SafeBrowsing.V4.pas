{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Provides access to the Google Safe Browsing API)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Added threatLists method
// 2023-03-16  pk  ---  Refactored into multiple units
// 2023-03-15  pk  ---  First draft with working threatMatches:find method
// *****************************************************************************
   )
}

unit API.Google.SafeBrowsing.V4;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch TypeHelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Layers.Transport.Base,
   Layers.JSON.Base,
   API.Google.SafeBrowsing.V4.Types,
   API.Google.SafeBrowsing.V4.ThreatLists,
   API.Google.SafeBrowsing.V4.ThreatMatches;

type

   {
      TSafeBrowsingV4API is the main class implementing access to the various
      functions related to Googles Safe Browsing API (version 4).

      Use of this class requires OpenSSL 3 libraries to be present.
   }

   TSafeBrowsingV4API = class
   private
      FAPIKey: string;
      FClientId: string;
      FClientVersion: string;
      FProxyHost: string;
      FProxyPort: integer;
   protected
      function CreateBaseQuery: TLayeredJSONObject;
      function CreateThreatMatchesFindURLsBody(AURLs: array of string; AThreatTypes: TSafeBrowsingThreatTypes; APlatforms: TSafeBrowsingPlatformTypes;
         AThreatEntryTypes: TSafeBrowsingThreatEntryTypes): TLayeredJSONObject;
   public
      constructor Create;
      {
         ThreatMatchesFindURLs checks a list of up to 500 URLs against the
         specified Safe Browsing lists.

         @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/threatMatches/find
      }
      function ThreatMatchesFindURLs(AURLs: array of string; const TheMatches: TThreatMatches; AThreatTypes: TSafeBrowsingThreatTypes = [sbttMalware, sbttSocialEngineering];
         APlatforms: TSafeBrowsingPlatformTypes = [sbptAnyPlatform]; AThreatEntryTypes: TSafeBrowsingThreatEntryTypes = [sbtetURL]): boolean;
      function ThreatListsList(const TheLists: TThreatLists): boolean;
      property ClientId: string read FClientId write FClientId;
      property ClientVersion: string read FClientVersion write FClientVersion;
      {
         APIKey needs to be set to a Google Cloud API key that is allowed
         to use the Safe Browsing API (v4, not legacy).
         Check the README.md for details on obtaining one.
      }
      property APIKey: string read FAPIKey write FAPIKey;
      property ProxyHost: string read FProxyHost write FProxyHost;
      property ProxyPort: integer read FProxyPort write FProxyPort;
   end;

implementation

{ TSafeBrowsingV4API }

function TSafeBrowsingV4API.CreateBaseQuery: TLayeredJSONObject;
var
   oClient: TLayeredJSONObject;
begin
   Result := TLayeredJSONObject.CreateInstance(True);
   oClient := TLayeredJSONObject.CreateInstance(True);
   try
      oClient.AddString('clientId', Self.ClientId);
      oClient.AddString('clientVersion', Self.ClientVersion);
      Result.AddObject('client', oClient);
   finally
      oClient.Free;
   end;
end;

function TSafeBrowsingV4API.CreateThreatMatchesFindURLsBody(AURLs: array of string; AThreatTypes: TSafeBrowsingThreatTypes; APlatforms: TSafeBrowsingPlatformTypes;
   AThreatEntryTypes: TSafeBrowsingThreatEntryTypes): TLayeredJSONObject;
var
   oThreatInfo: TLayeredJSONObject;
   jaThreatTypes: TLayeredJSONArray;
   jaPlatforms: TLayeredJSONArray;
   jaThreatEntryTypes: TLayeredJSONArray;
   aThreatEntries: TLayeredJSONArray;
   oThreatEntry: TLayeredJSONObject;
   s: string;
   tt: TSafeBrowsingThreatType;
   pt: TSafeBrowsingPlatformType;
   tet: TSafeBrowsingThreatEntryType;
begin
   Result := CreateBaseQuery;
   oThreatInfo := TLayeredJSONObject.CreateInstance(True);
   try
      jaThreatTypes := TLayeredJSONArray.CreateInstance(True);
      try
         for tt in AThreatTypes do begin
            jaThreatTypes.AddString(tt.ToString);
         end;
         oThreatInfo.AddArray('threatTypes', jaThreatTypes);
      finally
         jaThreatTypes.Free;
      end;
      jaPlatforms := TLayeredJSONArray.CreateInstance(True);
      try
         for pt in APlatforms do begin
            jaPlatforms.AddString(pt.ToString);
         end;
         oThreatInfo.AddArray('platformTypes', jaPlatforms);
      finally
         jaPlatforms.Free;
      end;
      jaThreatEntryTypes := TLayeredJSONArray.CreateInstance(True);
      try
         for tet in AThreatEntryTypes do begin
            jaThreatEntryTypes.AddString(tet.ToString);
         end;
         oThreatInfo.AddArray('threatEntryTypes', jaThreatEntryTypes);
      finally
         jaThreatEntryTypes.Free;
      end;
      aThreatEntries := TLayeredJSONArray.CreateInstance(True);
      try
         for s in AURLs do begin
            oThreatEntry := TLayeredJSONObject.CreateInstance(True);
            try
               oThreatEntry.AddString('url', s);
               aThreatEntries.AddObject(oThreatEntry);
            finally
               oThreatEntry.Free;
            end;
         end;
         oThreatInfo.AddArray('threatEntries', aThreatEntries);
      finally
         aThreatEntries.Free;
      end;
      Result.AddObject('threatInfo', oThreatInfo);
   finally
      oThreatInfo.Free;
   end;
end;

constructor TSafeBrowsingV4API.Create;
begin
   Self.ClientId := 'org.safer-networking.api.google.safebrowsing.v4';
   Self.ClientVersion := '0.1.0.0';
   Self.FProxyHost := '';
   Self.ProxyPort := 0;
end;

function TSafeBrowsingV4API.ThreatMatchesFindURLs(AURLs: array of string; const TheMatches: TThreatMatches; AThreatTypes: TSafeBrowsingThreatTypes;
   APlatforms: TSafeBrowsingPlatformTypes; AThreatEntryTypes: TSafeBrowsingThreatEntryTypes): boolean;
var
   sURL: string;
   http: TLayeredTransport;
   ms: TMemoryStream;
   oInput: TLayeredJSONObject;
   oResponse: TLayeredJSONObject;
begin
   Result := False;
   if not Assigned(TheMatches) then begin
      SafeBrowsingResultObjectMissingException.Create('Result object is missing.');
   end;
   if Length(AURLs) > 500 then begin
      raise SafeBrowsingTooManyURLsException.Create('Too many URLs specified; Google allows up to 500 in Safe Browsing API v4');
   end;
   sURL := 'https://safebrowsing.googleapis.com/v4/threatMatches:find?key=' + Self.APIKey;
   http := TLayeredTransport.CreateInstance;
   try
      http.ContentType := 'application/json';
      http.ProxyHost := Self.ProxyHost;
      http.ProxyPort := Self.ProxyPort;
      ms := TMemoryStream.Create;
      try
         oInput := CreateThreatMatchesFindURLsBody(AURLs, AThreatTypes, APlatforms, AThreatEntryTypes);
         try
            oResponse := TLayeredJSONObject.CreateInstance(False);
            try
               Result := http.POST(sURL, oInput, oResponse);
               if Result then begin
                  Result := TheMatches.LoadFromJSON(oResponse);
               end;
            finally
               oResponse.FreeObject;
               oResponse.Free;
            end;
         finally
            oInput.FreeObject;
            oInput.Free;
         end;
      finally
         ms.Free;
      end;
   finally
      http.Free;
   end;
end;

function TSafeBrowsingV4API.ThreatListsList(const TheLists: TThreatLists): boolean;
var
   sURL: string;
   o: TLayeredJSONObject;
   http: TLayeredTransport;
begin
   if not Assigned(TheLists) then begin
      SafeBrowsingResultObjectMissingException.Create('Result object is missing.');
   end;
   http := TLayeredTransport.CreateInstance;
   try
      http.ContentType := 'application/json';
      http.ProxyHost := Self.ProxyHost;
      http.ProxyPort := Self.ProxyPort;
      sURL := 'https://safebrowsing.googleapis.com/v4/threatLists?key=' + Self.APIKey;
      o := TLayeredJSONObject.CreateInstance(False);
      try
         Result := http.GET(sURL, o);
         if Result then begin
            Result := TheLists.LoadFromJSON(o);
         end;
      finally
         o.FreeObject;
         o.Free;
      end;
   finally
      http.Free;
   end;
end;

end.
