{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Defines Safe Browsing data types.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-15  pk  ---  Created with type definitions from documentation.
// *****************************************************************************
   )
}

unit API.Google.SafeBrowsing.V4.Types;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch TypeHelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Layers.Transport.Base;

type

   SafeBrowsingException = class(Exception);
   SafeBrowsingTooManyURLsException = class(SafeBrowsingException);
   SafeBrowsingResultObjectMissingException = class(SafeBrowsingException);
   SafeBrowsingJSONParsingException = class(SafeBrowsingException);

type
   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/ThreatType
   }
   TSafeBrowsingThreatType = (sbttUnspecified, sbttMalware, sbttSocialEngineering, sbttUnwantedSoftware, sbttPotentiallyHarmfulApplication);
   TSafeBrowsingThreatTypes = set of TSafeBrowsingThreatType;

   { TSafeBrowsingThreatTypeHelper }

   TSafeBrowsingThreatTypeHelper = type helper for TSafeBrowsingThreatType
      function ToString: string;
      function FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean = True): boolean;
   end;

   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/PlatformType
   }
   TSafeBrowsingPlatformType = (sbptUnspecified, sbptWindows, sbptLinux, sbptAndroid, sbptOSX, sbptIOS, sbptAnyPlatform, sbtAllPlatfors, sbptChrome);
   TSafeBrowsingPlatformTypes = set of TSafeBrowsingPlatformType;

   { TSafeBrowsingPlatformTypeHelper }

   TSafeBrowsingPlatformTypeHelper = type helper for TSafeBrowsingPlatformType
      function ToString: string;
      function FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean = True): boolean;
   end;

   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/ThreatEntryType
   }
   TSafeBrowsingThreatEntryType = (sbtetUnspecified, sbtetURL, sbtetExecutable, sbtetIPRange);
   TSafeBrowsingThreatEntryTypes = set of TSafeBrowsingThreatEntryType;

   { TSafeBrowsingThreatEntryTypeHelper }

   TSafeBrowsingThreatEntryTypeHelper = type helper for TSafeBrowsingThreatEntryType
      function ToString: string;
      function FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean = True): boolean;
   end;

const
   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/ThreatType
   }
   SSafeBrowsingThreatType: array[TSafeBrowsingThreatType] of string = ('THREAT_TYPE_UNSPECIFIED', 'MALWARE', 'SOCIAL_ENGINEERING', 'UNWANTED_SOFTWARE', 'POTENTIALLY_HARMFUL_APPLICATION');

   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/PlatformType
   }
   SSafeBrowsingPlatformType: array[TSafeBrowsingPlatformType] of string =
      ('PLATFORM_TYPE_UNSPECIFIED', 'WINDOWS', 'LINUX', 'ANDROID', 'OSX', 'IOS', 'ANY_PLATFORM', 'ALL_PLATFORMS', 'CHROME');

   {
      @link https://developers.google.com/safe-browsing/v4/reference/rest/v4/ThreatEntryType
   }
   SSafeBrowsingThreatEntryType: array[TSafeBrowsingThreatEntryType] of string = ('THREAT_ENTRY_TYPE_UNSPECIFIED', 'URL', 'EXECUTABLE', 'IP_RANGE');

implementation

{ TSafeBrowsingThreatEntryTypeHelper }

function TSafeBrowsingThreatEntryTypeHelper.ToString: string;
begin
   Result := SSafeBrowsingThreatEntryType[Self];
end;

function TSafeBrowsingThreatEntryTypeHelper.FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean): boolean;
var
   tet: TSafeBrowsingThreatEntryType;
begin
   for tet in TSafeBrowsingThreatEntryType do begin
      if SameText(TheString, SSafeBrowsingThreatEntryType[tet]) then begin
         Self := tet;
         Result := True;
         Exit;
      end;
   end;
   if ASetAsUnspecifiedIfMissing then begin
      Self := sbtetUnspecified;
   end;
   Result := False;
end;

{ TSafeBrowsingPlatformTypeHelper }

function TSafeBrowsingPlatformTypeHelper.ToString: string;
begin
   Result := SSafeBrowsingPlatformType[Self];
end;

function TSafeBrowsingPlatformTypeHelper.FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean): boolean;
var
   pt: TSafeBrowsingPlatformType;
begin
   for pt in TSafeBrowsingPlatformType do begin
      if SameText(TheString, SSafeBrowsingPlatformType[pt]) then begin
         Self := pt;
         Result := True;
         Exit;
      end;
   end;
   if ASetAsUnspecifiedIfMissing then begin
      Self := sbptUnspecified;
   end;
   Result := False;
end;

{ TSafeBrowsingThreatTypeHelper }

function TSafeBrowsingThreatTypeHelper.ToString: string;
begin
   Result := SSafeBrowsingThreatType[Self];
end;

function TSafeBrowsingThreatTypeHelper.FromString(TheString: string; ASetAsUnspecifiedIfMissing: boolean): boolean;
var
   tt: TSafeBrowsingThreatType;
begin
   for tt in TSafeBrowsingThreatType do begin
      if SameText(TheString, SSafeBrowsingThreatType[tt]) then begin
         Self := tt;
         Result := True;
         Exit;
      end;
   end;
   if ASetAsUnspecifiedIfMissing then begin
      Self := sbttUnspecified;
   end;
   Result := False;
end;

end.
