{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Classes for handling data returned by threatLists method.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Created classes.
// *****************************************************************************
   )
}

unit API.Google.SafeBrowsing.V4.ThreatLists;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   Layers.JSON.Base,
   Layers.Transport.Base,
   API.Google.SafeBrowsing.V4.Types;

type

   { TThreatList }

   TThreatList = class
   private
      FPlatformType: TSafeBrowsingPlatformType;
      FThreatEntryType: TSafeBrowsingThreatEntryType;
      FThreatType: TSafeBrowsingThreatType;
   public
      function LoadFromJSON(TheObject: TLayeredJSONObject): boolean;
   published
      property ThreatType: TSafeBrowsingThreatType read FThreatType;
      property PlatformType: TSafeBrowsingPlatformType read FPlatformType;
      property ThreatEntryType: TSafeBrowsingThreatEntryType read FThreatEntryType;
   end;

   { TThreatLists }

   TThreatLists = class(TObjectList<TThreatList>)
   public
      function LoadFromJSON(AnObject: TLayeredJSONObject): boolean;
   end;

implementation

{ TThreatLists }

function TThreatLists.LoadFromJSON(AnObject: TLayeredJSONObject): boolean;
var
   aLists: TLayeredJSONArray;
   oMatch: TLayeredJSONObject;
   i: integer;
   tl: TThreatList;
begin
   Result := False;
   Self.Clear;

   if not AnObject.FindArray('threatLists', aLists) then begin
      raise SafeBrowsingJSONParsingException.Create('SafeBrowsing API result did not have the threatLists array.');
   end;
   try
      Result := True;
      for i := 0 to Pred(aLists.Count) do begin
         try
            oMatch := aLists.Objects[i];
            try
               tl := TThreatList.Create;
               if not tl.LoadFromJSON(oMatch) then begin
                  Result := False;
                  Exit;
               end;
            finally
               oMatch.Free;
            end;
            Self.Add(tl);
         except
            on E: Exception do begin
               Result := False;
               raise SafeBrowsingJSONParsingException.Create('SafeBrowsing threat match is not an object.');
            end;
         end;
      end;
   finally
      aLists.Free;
   end;
end;

{ TThreatList }

function TThreatList.LoadFromJSON(TheObject: TLayeredJSONObject): boolean;
var
   s: String;
begin
   Result := True;
   if TheObject.FindString('threatType', s) then begin
      Self.FThreatType.FromString(s, True);
   end else begin
      raise SafeBrowsingJSONParsingException.Create('SafeBrowsing ThreatMatch did not specify threatType.');
   end;
   if TheObject.FindString('platformType', s) then begin
      Self.FPlatformType.FromString(s, True);
   end else begin
      raise SafeBrowsingJSONParsingException.Create('SafeBrowsing ThreatMatch did not specify platformType.');
   end;
   if TheObject.FindString('threatEntryType', s) then begin
      Self.FThreatEntryType.FromString(s, True);
   end else begin
      raise SafeBrowsingJSONParsingException.Create('SafeBrowsing ThreatMatch did not specify threatEntryType.');
   end;
end;

end.
