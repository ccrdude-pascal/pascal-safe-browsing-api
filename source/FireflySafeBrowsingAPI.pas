{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflySafeBrowsingAPI;

{$warn 5023 off : no warning about unused units}
interface

uses
  API.Google.SafeBrowsing.V4, API.Google.SafeBrowsing.V4.ThreatLists, 
  API.Google.SafeBrowsing.V4.ThreatMatches, API.Google.SafeBrowsing.V4.Types, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflySafeBrowsingAPI', @Register);
end.
